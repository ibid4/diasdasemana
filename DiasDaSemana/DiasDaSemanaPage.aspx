﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiasDaSemanaPage.aspx.cs" Inherits="DiasDaSemana.DiasDaSemanaPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        
        <div>
            <h4>Selecione os dias que você poderá trabalhar:</h4>   
            <asp:CheckBoxList ID="chkDiasSemana" runat="server">
                <asp:ListItem Text="Segunda-feira" Value="Segunda-feira" />
                <asp:ListItem Text="Terça-feira" Value="Terça-feira" />
                <asp:ListItem Text="Quarta-feira" Value="Quarta-feira" />
                <asp:ListItem Text="Quinta-feira" Value="Quinta-feira" />
                <asp:ListItem Text="Sexta-feira" Value="Sexta-feira" />
                <asp:ListItem Text="Sábado" Value="Sábado" />
                <asp:ListItem Text="Domingo" Value="Domingo" />
            </asp:CheckBoxList>
            <asp:Button ID="btnMostrarSelecao" runat="server" Text="Mostrar Seleção" OnClick="btnMostrarSelecao_Click" />
            <p>Seleção: <asp:Label ID="lblSelecao" runat="server" Text=""></asp:Label></p>
        </div>
    </form>
</body>
</html>
