﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiasDaSemana
{
    public partial class DiasDaSemanaPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnMostrarSelecao_Click(object sender, EventArgs e)
        {
            string selecao = "Dias selecionados: ";

            // Itere sobre os itens selecionados do CheckBoxList
            foreach (ListItem item in chkDiasSemana.Items)
            {
                if (item.Selected)
                {
                    selecao += item.Value + ", ";
                }
            }

            
            selecao = selecao.TrimEnd(' ', ',');

            lblSelecao.Text = selecao;
        }
    }
}